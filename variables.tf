variable "region" {
  type        = string
  description = "aws region"
  default     = "us-east-1"

}

variable "instance_type" {
  type        = string
  description = "instance type"
  default     = "t2.micro"

}

variable "ami" {
  type        = string
  description = "image Linux"
  default     = "ami-01cc34ab2709337aa"
}