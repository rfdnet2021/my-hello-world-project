data "aws_vpc" "default" {
  #cidr_block           = "10.0.0.0/16"
  default = true

}



data "aws_subnet_ids" "subnet" {
  vpc_id = data.aws_vpc.default.id

}




/*
resource "aws_subnet" "dev_public_1a_subnet" {
  vpc_id                  = aws_vpc.default.id
  cidr_block              = "10.1.0.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"
  tags = {
    Name = "dev-public-1a"
  }
}

resource "aws_subnet" "dev_public_1b_subnet" {
  vpc_id                  = aws_vpc.default.id
  cidr_block              = "10.2.0.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1b"
  tags = {
    Name = "dev-public-1b"
  }
}
*/