resource "aws_security_group" "web-server" {
  name        = "web-server"
  description = "Allow incoming HTTP traffic"


  ingress {

    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["201.26.68.161/32"] #need to improve this!!
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}