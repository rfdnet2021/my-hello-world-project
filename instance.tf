#keypair for instance
# use my pub key from my home dir!
#
resource "aws_key_pair" "webserver_auth" {
  key_name   = "drosophila_key"
  public_key = file("~/.ssh/droso-dev.pub")

}

#webserver creation
resource "aws_instance" "web-server" {
  ami             = var.ami
  instance_type   = var.instance_type
  count           = 2
  key_name        = aws_key_pair.webserver_auth.id
  security_groups = ["${aws_security_group.web-server.name}"]
  user_data       = <<EOF
		#! /bin/bash
        sudo su 
        sudo yum update -y 
        sudo yum install httpd -y
		echo "<h1>My super hello world application responding from $(hostname -f) </h1>" | sudo tee /var/www/html/index.html
        systemctl start httpd
        systemctl enable httpd
	EOF

  tags = {
    name = "instance-${count.index}"
  }
}