
#Target group

resource "aws_lb_target_group" "target-group" {


  name        = "reinaldo-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = data.aws_subnet_ids.subnet.id #vpc_id      = aws_vpc.default.id

  health_check {
    interval            = 10
    path                = "/"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }

}

#Load Balancer
resource "aws_lb" "application-lb" {
  name               = "reinaldo-alb"
  internal           = false
  ip_address_type    = "ipv4"
  load_balancer_type = "application"
  security_groups    = [aws_security_group.web-server.id]
  #subnets                    = [aws_subnet.dev_public_1a_subnet.id,aws_subnet.dev_public_1b_subnet.id]
  subnets                    = data.aws_subnet_ids.subnet.ids
  enable_deletion_protection = false


  tags = {
    Name = "reinaldo-alb"
  }


}

# Listerner
resource "aws_lb_listener" "alb-Listerner" {
  load_balancer_arn = aws_lb.application-lb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.target-group.arn
    type             = "forward"
  }
}

#attachment
resource "aws_lb_target_group_attachment" "ec2_attach" {
  count            = length(aws_instance.web-server)
  target_group_arn = aws_lb_target_group.target-group.arn
  target_id        = aws_instance.web-server[count.index].id

}

